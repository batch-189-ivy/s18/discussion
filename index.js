// Function parameters are variables that wait for a value from an argument in the function invocation
function sayMyName(name){
	console.log('My name is ' + name)
}

// Function arguments are values like strings, numbers, numbers etc that you can pass onto the function you are invoking
sayMyName('Yor')


// You can re-use functions by invoking them at any time or any part of your code. Make sure you have declared them first
sayMyName('Loiloi')
sayMyName('Bond')

// You can also pass variable as arguments to a function
let myName = 'Anya'
sayMyName(myName)


// You can use arguments and parameters to make the data inside your functions dynamic
function sumOfTwoNumers(firstNumber, secondNumber){
	let sum = 0

	sum = firstNumber + secondNumber

	console.log(sum)
}

sumOfTwoNumers(10,15)
sumOfTwoNumers(99,1)
sumOfTwoNumers(50,20)


// You can pass a function as an argument for another function. Do not pass that function with parenthesis, only with its function name 
function argumentFunction(){
	console.log('This is a function that was passed as an argument')
}

function parameterFunction(argumentFunction){
	argumentFunction()
}

parameterFunction(argumentFunction)


// REAL WORLD APPLICATION

/*
	Imagine a product opoage where you have an 'add to cart button'
*/

function addProductToCart(){
	console.log('Click me to add product to cart')
}



let addToCartBtn = document.querySelector('#add-to-cart-btn')


addToCartBtn.addEventListener('click', addProductToCart)



function displayFullName(firstName, middleInitial, lastName, extraText){
	console.log('Your full name is: ' + firstName + ' ' + middleInitial + ' ' + lastName + ' ' + extraText)
}

//displayFullName('Earl', 'D', 'Diaz')
displayFullName('Johnny', 'B', 'Goode', 'Yeah')


function displayMovieDetails (title, synopsis, director) {
	console.log(`The movie is titled ${title}`)
	console.log(`The synopsis is  ${synopsis}`)
	console.log(`The director is  ${director}`)
}

displayMovieDetails('Sisterakas', 'I forgot the synopsis', 'Wenn Deramas')


console.log('Congrats to the Golden State Warriors!')
console.log("My money don't jiggle jiggle")

function displayPlayerdetails(name, age, playerClass){
	//console.log(`Player name: ${name}`)
	//console.log(`Player age: ${age}`)
	//console.log(`Player playerClass: ${playerClass}`)
	let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`

	return playerDetails

	console.log('Hello')
}

let variable = 'a variable'

console.log(displayPlayerdetails('Elsworth', 120, 'Paladin'))
